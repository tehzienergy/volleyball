






$(window).on('load resize', function () {
  var windowWidth = $(window).width()

  if (windowWidth < 992) {
    $('.front__arrow').on('click', function (e) {
      e.preventDefault()
      $('html, body').animate({
        scrollTop: $('[data-anchor="section2"]').offset().top
      }, 800);
    })
  }
})

$('.input').change(function() {
  if ($(this).val()) {
    $(this).addClass('input--active');
  }
  else {
    $(this).removeClass('input--active');
  }
})




$(window).on('load resize', function () {
  
  var windowWidth = $(window).width()
  
  if (windowWidth >= 992) {
    var fullPage = new fullpage('.main', {
      anchors: ['section1', 'section2', 'section3', 'section4', 'section5'],
      navigation: true,
      parallax: false,
      licenseKey: '84C8AD90-61A744E6-A33F2618-4CF51B1B',
      scrollOverflow: true,
      verticalCentered: false,
      navigationPosition: 'left',
      scrollOverflowOptions: {
        disablePointer: true,
      },
      /*onLeave: function(origin, destination, direction) {
        if (destination.isLast) {
          fullPage.setAutoScrolling(false);
          fullPage.setFitToSection(false);
        } else {
          fullPage.setAutoScrolling(true);
          fullPage.setFitToSection(true);
        }
      },*/
    })
  }
  
})



$('.menu__btn').click(function(e) {
  e.preventDefault();
  // $(this).toggle();
  $('.menu__btn--active').toggle();
  $('.menu__content').toggleClass('menu__content--active');
  $('body').toggleClass('overflow-is-hidden');
})





jQuery(document).ready(function($) {
  
  $('.slider').slick({
    centerMode: true,
    centerPadding: '0',
    slidesToShow: 1,
    arrows: true,
    dots: false,
    responsive: [
      {
        breakpoint: 1200,
        settings: {
          arrows: false,
          dots: true
        }
      },
    ]
  })
  
})

new SlimSelect({
  select: '.select__input',
  showSearch: false,
  placeholder: true
})





$('.submenu__link--active').click(function(e) {
  e.preventDefault();
  $(this).closest('.submenu__content').toggleClass('submenu__content--active');
})




//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImNhcmQvY2FyZC5qcyIsImFydGljbGUvYXJ0aWNsZS5qcyIsImNpdHkvY2l0eS5qcyIsImRvY3VtZW50L2RvY3VtZW50LmpzIiwiY2l0aWVzL2NpdGllcy5qcyIsImRvY3VtZW50YXRpb24vZG9jdW1lbnRhdGlvbi5qcyIsImVycm9yL2Vycm9yLmpzIiwiZnJvbnQvZnJvbnQuanMiLCJmb3JtL2Zvcm0uanMiLCJmb290ZXIvZm9vdGVyLmpzIiwiaGVhZGVyL2hlYWRlci5qcyIsImlucHV0L2lucHV0LmpzIiwibWFpbi9tYWluLmpzIiwibG9nby9sb2dvLmpzIiwiam9iL2pvYi5qcyIsIm1lbnUvbWVudS5qcyIsInBlcnNvbi9wZXJzb24uanMiLCJzZWN0aW9uL3NlY3Rpb24uanMiLCJwaG90by9waG90by5qcyIsIm5ld3MvbmV3cy5qcyIsInNsaWRlci9zbGlkZXIuanMiLCJzZWxlY3Qvc2VsZWN0LmpzIiwic2lkZWxpbmtzL3NpZGVsaW5rcy5qcyIsInNvY2lhbHMvc29jaWFscy5qcyIsInNpZGViYXIvc2lkZWJhci5qcyIsInN1YmxvZ28vc3VibG9nby5qcyIsInN1Ym1lbnUvc3VibWVudS5qcyIsInN1YnNjcmliZS9zdWJzY3JpYmUuanMiLCJ2YWNhbmN5L3ZhY2FuY3kuanMiLCJ3cmFwcGVyL3dyYXBwZXIuanMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7QUNBQTtBQ0FBO0FDQUE7QUNBQTtBQ0FBO0FDQUE7QUNBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQ1pBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQ1JBO0FDQUE7QUNBQTtBQ0FBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQzdCQTtBQ0FBO0FDQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQ1BBO0FDQUE7QUNBQTtBQ0FBO0FDQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FDcEJBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQ0xBO0FDQUE7QUNBQTtBQ0FBO0FDQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQ0pBO0FDQUE7QUNBQSIsImZpbGUiOiJzY3JpcHRzLmpzIiwic291cmNlc0NvbnRlbnQiOlsiIiwiIiwiIiwiIiwiIiwiIiwiIiwiJCh3aW5kb3cpLm9uKCdsb2FkIHJlc2l6ZScsIGZ1bmN0aW9uICgpIHtcclxuICB2YXIgd2luZG93V2lkdGggPSAkKHdpbmRvdykud2lkdGgoKVxyXG5cclxuICBpZiAod2luZG93V2lkdGggPCA5OTIpIHtcclxuICAgICQoJy5mcm9udF9fYXJyb3cnKS5vbignY2xpY2snLCBmdW5jdGlvbiAoZSkge1xyXG4gICAgICBlLnByZXZlbnREZWZhdWx0KClcclxuICAgICAgJCgnaHRtbCwgYm9keScpLmFuaW1hdGUoe1xyXG4gICAgICAgIHNjcm9sbFRvcDogJCgnW2RhdGEtYW5jaG9yPVwic2VjdGlvbjJcIl0nKS5vZmZzZXQoKS50b3BcclxuICAgICAgfSwgODAwKTtcclxuICAgIH0pXHJcbiAgfVxyXG59KVxyXG4iLCIkKCcuaW5wdXQnKS5jaGFuZ2UoZnVuY3Rpb24oKSB7XHJcbiAgaWYgKCQodGhpcykudmFsKCkpIHtcclxuICAgICQodGhpcykuYWRkQ2xhc3MoJ2lucHV0LS1hY3RpdmUnKTtcclxuICB9XHJcbiAgZWxzZSB7XHJcbiAgICAkKHRoaXMpLnJlbW92ZUNsYXNzKCdpbnB1dC0tYWN0aXZlJyk7XHJcbiAgfVxyXG59KVxyXG4iLCIiLCIiLCIiLCIkKHdpbmRvdykub24oJ2xvYWQgcmVzaXplJywgZnVuY3Rpb24gKCkge1xyXG4gIFxyXG4gIHZhciB3aW5kb3dXaWR0aCA9ICQod2luZG93KS53aWR0aCgpXHJcbiAgXHJcbiAgaWYgKHdpbmRvd1dpZHRoID49IDk5Mikge1xyXG4gICAgdmFyIGZ1bGxQYWdlID0gbmV3IGZ1bGxwYWdlKCcubWFpbicsIHtcclxuICAgICAgYW5jaG9yczogWydzZWN0aW9uMScsICdzZWN0aW9uMicsICdzZWN0aW9uMycsICdzZWN0aW9uNCcsICdzZWN0aW9uNSddLFxyXG4gICAgICBuYXZpZ2F0aW9uOiB0cnVlLFxyXG4gICAgICBwYXJhbGxheDogZmFsc2UsXHJcbiAgICAgIGxpY2Vuc2VLZXk6ICc4NEM4QUQ5MC02MUE3NDRFNi1BMzNGMjYxOC00Q0Y1MUIxQicsXHJcbiAgICAgIHNjcm9sbE92ZXJmbG93OiB0cnVlLFxyXG4gICAgICB2ZXJ0aWNhbENlbnRlcmVkOiBmYWxzZSxcclxuICAgICAgbmF2aWdhdGlvblBvc2l0aW9uOiAnbGVmdCcsXHJcbiAgICAgIHNjcm9sbE92ZXJmbG93T3B0aW9uczoge1xyXG4gICAgICAgIGRpc2FibGVQb2ludGVyOiB0cnVlLFxyXG4gICAgICB9LFxyXG4gICAgICAvKm9uTGVhdmU6IGZ1bmN0aW9uKG9yaWdpbiwgZGVzdGluYXRpb24sIGRpcmVjdGlvbikge1xyXG4gICAgICAgIGlmIChkZXN0aW5hdGlvbi5pc0xhc3QpIHtcclxuICAgICAgICAgIGZ1bGxQYWdlLnNldEF1dG9TY3JvbGxpbmcoZmFsc2UpO1xyXG4gICAgICAgICAgZnVsbFBhZ2Uuc2V0Rml0VG9TZWN0aW9uKGZhbHNlKTtcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgZnVsbFBhZ2Uuc2V0QXV0b1Njcm9sbGluZyh0cnVlKTtcclxuICAgICAgICAgIGZ1bGxQYWdlLnNldEZpdFRvU2VjdGlvbih0cnVlKTtcclxuICAgICAgICB9XHJcbiAgICAgIH0sKi9cclxuICAgIH0pXHJcbiAgfVxyXG4gIFxyXG59KVxyXG4iLCIiLCIiLCIkKCcubWVudV9fYnRuJykuY2xpY2soZnVuY3Rpb24oZSkge1xyXG4gIGUucHJldmVudERlZmF1bHQoKTtcclxuICAvLyAkKHRoaXMpLnRvZ2dsZSgpO1xyXG4gICQoJy5tZW51X19idG4tLWFjdGl2ZScpLnRvZ2dsZSgpO1xyXG4gICQoJy5tZW51X19jb250ZW50JykudG9nZ2xlQ2xhc3MoJ21lbnVfX2NvbnRlbnQtLWFjdGl2ZScpO1xyXG4gICQoJ2JvZHknKS50b2dnbGVDbGFzcygnb3ZlcmZsb3ctaXMtaGlkZGVuJyk7XHJcbn0pXHJcbiIsIiIsIiIsIiIsIiIsImpRdWVyeShkb2N1bWVudCkucmVhZHkoZnVuY3Rpb24oJCkge1xyXG4gIFxyXG4gICQoJy5zbGlkZXInKS5zbGljayh7XHJcbiAgICBjZW50ZXJNb2RlOiB0cnVlLFxyXG4gICAgY2VudGVyUGFkZGluZzogJzAnLFxyXG4gICAgc2xpZGVzVG9TaG93OiAxLFxyXG4gICAgYXJyb3dzOiB0cnVlLFxyXG4gICAgZG90czogZmFsc2UsXHJcbiAgICByZXNwb25zaXZlOiBbXHJcbiAgICAgIHtcclxuICAgICAgICBicmVha3BvaW50OiAxMjAwLFxyXG4gICAgICAgIHNldHRpbmdzOiB7XHJcbiAgICAgICAgICBhcnJvd3M6IGZhbHNlLFxyXG4gICAgICAgICAgZG90czogdHJ1ZVxyXG4gICAgICAgIH1cclxuICAgICAgfSxcclxuICAgIF1cclxuICB9KVxyXG4gIFxyXG59KVxyXG4iLCJuZXcgU2xpbVNlbGVjdCh7XHJcbiAgc2VsZWN0OiAnLnNlbGVjdF9faW5wdXQnLFxyXG4gIHNob3dTZWFyY2g6IGZhbHNlLFxyXG4gIHBsYWNlaG9sZGVyOiB0cnVlXHJcbn0pXHJcbiIsIiIsIiIsIiIsIiIsIiQoJy5zdWJtZW51X19saW5rLS1hY3RpdmUnKS5jbGljayhmdW5jdGlvbihlKSB7XHJcbiAgZS5wcmV2ZW50RGVmYXVsdCgpO1xyXG4gICQodGhpcykuY2xvc2VzdCgnLnN1Ym1lbnVfX2NvbnRlbnQnKS50b2dnbGVDbGFzcygnc3VibWVudV9fY29udGVudC0tYWN0aXZlJyk7XHJcbn0pXHJcbiIsIiIsIiIsIiJdfQ==
