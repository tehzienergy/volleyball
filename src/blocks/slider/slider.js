jQuery(document).ready(function($) {
  
  $('.slider').slick({
    centerMode: true,
    centerPadding: '0',
    slidesToShow: 1,
    arrows: true,
    dots: false,
    responsive: [
      {
        breakpoint: 1200,
        settings: {
          arrows: false,
          dots: true
        }
      },
    ]
  })
  
})
