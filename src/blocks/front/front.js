$(window).on('load resize', function () {
  var windowWidth = $(window).width()

  if (windowWidth < 992) {
    $('.front__arrow').on('click', function (e) {
      e.preventDefault()
      $('html, body').animate({
        scrollTop: $('[data-anchor="section2"]').offset().top
      }, 800);
    })
  }
})
