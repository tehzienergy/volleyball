$(window).on('load resize', function () {
  
  var windowWidth = $(window).width()
  
  if (windowWidth >= 992) {
    var fullPage = new fullpage('.main', {
      anchors: ['section1', 'section2', 'section3', 'section4', 'section5'],
      navigation: true,
      parallax: false,
      licenseKey: '84C8AD90-61A744E6-A33F2618-4CF51B1B',
      scrollOverflow: true,
      verticalCentered: false,
      navigationPosition: 'left',
      scrollOverflowOptions: {
        disablePointer: true,
      },
      /*onLeave: function(origin, destination, direction) {
        if (destination.isLast) {
          fullPage.setAutoScrolling(false);
          fullPage.setFitToSection(false);
        } else {
          fullPage.setAutoScrolling(true);
          fullPage.setFitToSection(true);
        }
      },*/
    })
  }
  
})
